/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import org.alicebot.ab.Bot;
import org.alicebot.ab.Chat;
import org.alicebot.ab.MagicBooleans;

/**
 *
 * @author harry
 */
public class EmOi {
    private static Bot bot;
    private static Chat chatSession;
    private static String botName = "super";
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        StringBuilder text = new StringBuilder("");
        if(args == null || args.length == 0) {
            //return;
        }
        else {
            for(String each : args) {
                text = text.append(each).append(" ");
            }
        }
        File folder = new File("data");
        if(!folder.exists()) {
            return;
        }
        
        bot = new Bot(botName, folder.getAbsolutePath(), "chat");
        testChat(text.toString().trim());
    }
    
    public static void testChat (String text) {
        
        chatSession = new Chat(bot);
        bot.brain.nodeStats();
        MagicBooleans.fix_excel_csv = false;
        MagicBooleans.trace_mode = false;
        
        String response = chatSession.multisentenceRespond(text);
        System.out.println(response);
    }
}
